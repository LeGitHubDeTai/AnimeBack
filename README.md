![GitHub Logo](https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/logo%20white2.png?raw=true)
<p align='center'>
  <a href="https://legithubdetai.github.io/AnimeBack/">
    <img src="https://img.shields.io/github/downloads/LeGitHubDeTai/AnimeBack/total">
    <img src="https://img.shields.io/github/v/release/LeGitHubDeTai/AnimeBack">
    <img src="https://img.shields.io/website?url=http%3A%2F%2Flegithubdetai.github.io%2FAnimeBack">
    <img src="https://img.shields.io/github/release-date/LeGitHubDeTai/AnimeBack">
    <img src="https://img.shields.io/github/license/LeGitHubDeTai/AnimeBack">
  </a>
  <a href="https://discord.gg/zctFdAPUzP">
    <img src="https://img.shields.io/discord/788853994264723456">
  </a>
  <br/>
  <a href="http://www.youtube.com/watch?v=JpFKSTRth4M">
    <img src="https://img.shields.io/youtube/views/JpFKSTRth4M?style=social">
  </a>
  <a href="https://github.com/LeGitHubDeTai/">
    <img src="https://img.shields.io/github/followers/LeGitHubDeTai?style=social">
  </a>
  <a href="https://www.youtube.com/channel/UCZiVWB8_UNH4NLzr7XbaI8A">
    <img src="https://img.shields.io/youtube/channel/subscribers/UCZiVWB8_UNH4NLzr7XbaI8A?style=social">
  </a>
  <a href="https://github.com/LeGitHubDeTai/AnimeBack">
    <img src="https://img.shields.io/github/stars/LeGitHubDeTai/AnimeBack?style=social">
  </a>
</p>

<p align='center'>
  <a href="https://github.com/LeGitHubDeTai/AnimeBack/releases">
    <img src="https://img.shields.io/endpoint?url=https%3A%2F%2Fraw.githubusercontent.com%2FLeGitHubDeTai%2FAnimeBack%2Fmain%2Fcustom.json">
  </a>
</p>


## ⚙️ Create your extenions
If you are a developer and / or you want to create an extension
please read the [Wiki](https://github.com/LeGitHubDeTai/AnimeBack/wiki/Extensions).

## 🚀 Adding your extensions

If you have an Extensions you'd like to see added,
please read the [Contributions](https://github.com/TaiStudio/animeback-submit/blob/master/CONTRIBUTING.md) doc.

## 🔑 How it Works

See [Contributions.md#how-it-works](https://github.com/TaiStudio/animeback-submit/blob/master/CONTRIBUTING.md#how-it-works)

## 📜 License

MIT
<p align='center'>
  <a href="http://www.youtube.com/watch?v=JpFKSTRth4M">
    <img src="https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/Trailer%20Animeback.jpg?raw=true" width="45%" ></img>
  </a>
  
  <br/>
  
  <img src="https://raw.githubusercontent.com/LeGitHubDeTai/AnimeBack/main/assets/tray.png" width="45%" ></img>
  <img src="https://raw.githubusercontent.com/LeGitHubDeTai/AnimeBack/main/assets/changelog.png" width="45%" ></img>
  <img src="https://raw.githubusercontent.com/LeGitHubDeTai/AnimeBack/main/assets/desktop.png" width="45%" ></img>
  <img src="https://raw.githubusercontent.com/LeGitHubDeTai/AnimeBack/main/assets/options.png" width="45%" ></img>
  <img src="https://raw.githubusercontent.com/LeGitHubDeTai/AnimeBack/main/assets/wikigit.png" width="45%" ></img>
  <img src="https://raw.githubusercontent.com/LeGitHubDeTai/AnimeBack/main/assets/github.png" width="45%" ></img>
  
  <br/>
  
  <img src="https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/tray%20options.png?raw=true" width="45%" ></img>
  <img src="https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/options%20window.png?raw=true" width="45%" ></img>
  <img src="https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/changelog%20window.png?raw=true" width="45%" ></img>
  <img src="https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/add%20extensions.png?raw=true" width="45%" ></img>
  <img src="https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/add%20custom.png?raw=true" width="45%" ></img>
  <img src="https://github.com/LeGitHubDeTai/AnimeBack/blob/main/assets/add%20custom.png?raw=true" width="45%" ></img>
 </p>

